package com.commonwealth.crud.exception;

public class ExcUserNotFound extends RuntimeException {

    ExcUserNotFound(Long userId){
        super("User not found in database : "+userId);
    }
}
