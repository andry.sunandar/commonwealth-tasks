package com.commonwealth.crud.repository;

import com.commonwealth.crud.model.DoUser;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
public interface DaUser extends JpaRepository<DoUser,Long> {

    Optional<DoUser> findByName(String name);
    void deleteByName(String name);
}
