package com.commonwealth.crud.advice;

import com.commonwealth.crud.exception.ExcUserNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class AdUserNotFound {

    @ResponseBody
    @ExceptionHandler(ExcUserNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String UserNotFoundHandler(ExcUserNotFound ex){
        return ex.getMessage();
    }
}
