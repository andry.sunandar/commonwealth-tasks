package com.commonwealth.crud.triangle;

import java.util.Scanner;

public class TriangleApp {

    public static void main(String[] args) {

        System.out.println("=== PLEASE INPUT TOTAL LINE : ====");
        Scanner sc = new Scanner(System.in);
        while (true){
            String line = sc.nextLine().replaceAll("\n", "");
            String[] argsInput = line.split(" ");
            if (argsInput.length == 1) {
                int totalLine = Integer.parseInt(argsInput[0]);
                if (totalLine > 0 && totalLine <= 100) {
                    for (int x = 1; x <= totalLine; x++) {
                        for (int space = totalLine; space > x; space--)
                            System.out.print("  ");
                        for (int y = 1; y <= x; y++)
                            System.out.print("# ");
                        System.out.println();
                    }
                }else{
                    System.out.println("Please Input Number From 0 < n ≤ 100");
                }
            } else {
                System.out.println("Argument length more than 1");
            }
        }
    }
}
