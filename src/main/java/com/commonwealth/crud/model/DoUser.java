package com.commonwealth.crud.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "USERS")
@DynamicUpdate
public class DoUser implements Serializable {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private  Long id;
    @Column(unique = true)
    private String name;
    @Column(unique = true)
    private String phone;
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
