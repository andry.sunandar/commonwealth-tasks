package com.commonwealth.crud.controller;

import com.commonwealth.crud.model.DoUser;
import com.commonwealth.crud.repository.DaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(value = "/api/")
public class CtUser {

    @Autowired
    DaUser daUser;

    PasswordEncoder encoder = new BCryptPasswordEncoder();

    @PostMapping("v1/users/add")
    ResponseEntity<DoUser> newUser(@RequestBody DoUser newUser){
        newUser.setPassword(encoder.encode(newUser.getPassword()));
        return new ResponseEntity(daUser.save(newUser),HttpStatus.OK);
    }

    @GetMapping("v1/users/delete")
    ResponseEntity deleteUser(Principal principal){
        daUser.deleteByName(principal.getName());
        return new ResponseEntity(HttpStatus.OK);

    }

    @GetMapping("v1/users/read")
    ResponseEntity<DoUser> readUser(Principal principal){
        return new ResponseEntity(daUser.findByName(principal.getName()),HttpStatus.OK);

    }

    @PostMapping("v1/users/update")
    ResponseEntity<DoUser> updateUser(@RequestBody DoUser updateUser, Principal principal){
        DoUser oldUser = daUser.findByName(principal.getName())
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + principal.getName()));
        oldUser.setName(updateUser.getName());
        oldUser.setPhone(updateUser.getPhone());
        oldUser.setPassword(encoder.encode(updateUser.getPassword()));
        return new ResponseEntity(daUser.save(oldUser),HttpStatus.OK);

    }
}
