package com.commonwealth.crud.controller;

import com.commonwealth.crud.dto.DtJwtResp;
import com.commonwealth.crud.dto.DtUserDetailsImpl;
import com.commonwealth.crud.model.DoUser;
import com.commonwealth.crud.repository.DaUser;
import com.commonwealth.crud.utility.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/")
public class CtAuth {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    DaUser userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("v1/auth/getToken")
    public ResponseEntity<?> authenticateUser(@RequestBody DoUser loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getName(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        DtUserDetailsImpl userDetails = (DtUserDetailsImpl) authentication.getPrincipal();
        List<String> roles = Collections.singletonList("ROLE_USER");

        return ResponseEntity.ok(new DtJwtResp(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getPhone(),
                roles));
    }

}
