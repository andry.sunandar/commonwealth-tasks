package com.commonwealth.crud.service;

import com.commonwealth.crud.dto.DtUserDetailsImpl;
import com.commonwealth.crud.model.DoUser;
import com.commonwealth.crud.repository.DaUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ScUserDetailsImpl implements UserDetailsService {

    @Autowired
    DaUser daUser;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        DoUser user = daUser.findByName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
        return DtUserDetailsImpl.build(user);
    }

}
