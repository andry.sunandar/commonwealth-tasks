CREATE TABLE IF NOT EXISTS users (
    id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(50) UNIQUE,
    phone varchar(30) UNIQUE ,
    password varchar(140)
    );
INSERT INTO users (name,phone,password)
values ('toni','08131673617','$2a$10$cH8.e7HB5c.tk5SZDI2UO.fZQ4E0YR9knR7/d/e2u.xS8yLUA9S2y');

INSERT INTO users (name,phone,password)
values ('dudung','08131623423','$2a$10$x3w08ETTbo/wbCQlxiXoKeT8Q7hXvLGIn1qxVPjZW/M60No/LoHxO');